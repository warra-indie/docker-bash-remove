"use strict";

require("@babel/polyfill");

var _child_process = _interopRequireDefault(require("child_process"));

var _react = _interopRequireWildcard(require("react"));

var _ink = require("ink");

var _inkSelectInput = _interopRequireDefault(require("ink-select-input"));

var _containerHelpers = require("./container-helpers");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ACTIONS = [{
  label: 'bash into container',
  value: 'bash'
}, {
  label: 'remove container',
  value: 'remove'
}];

var Remove = /*#__PURE__*/function (_Component) {
  _inherits(Remove, _Component);

  function Remove() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Remove);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Remove)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      containers: undefined,
      containerNames: undefined,
      action: undefined,
      inChildProcess: undefined,
      currentContainerIndex: undefined
    });

    _defineProperty(_assertThisInitialized(_this), "onSelectAction", /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref2) {
        var label, value;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                label = _ref2.label, value = _ref2.value;
                console.log('value is', value);

                _this.setState({
                  action: value
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(_assertThisInitialized(_this), "onSelectContainer", /*#__PURE__*/function () {
      var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_ref4) {
        var label, value, action, activeContainer;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                label = _ref4.label, value = _ref4.value;
                action = _this.state.action;
                activeContainer = (0, _containerHelpers.getContainer)(value);

                if (!(action === 'remove')) {
                  _context2.next = 12;
                  break;
                }

                _context2.next = 6;
                return activeContainer.stop();

              case 6:
                _context2.next = 8;
                return activeContainer.remove();

              case 8:
                console.log("Removed Container!");
                process.exit();
                _context2.next = 14;
                break;

              case 12:
                process.on('exit', function () {
                  _this.setState({
                    inChildProcess: true
                  }, function () {
                    _child_process["default"].execSync("docker exec -it ".concat(value, " /bin/sh"), {
                      stdio: 'inherit'
                    });
                  });
                });
                process.exit();

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x2) {
        return _ref3.apply(this, arguments);
      };
    }());

    return _this;
  }

  _createClass(Remove, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var _ref5, containerData, containerNames, imageIndex;

        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return (0, _containerHelpers.getInitialContainerData)();

              case 2:
                _ref5 = _context3.sent;
                containerData = _ref5.containerData;
                containerNames = _ref5.containerNames;
                imageIndex = _ref5.imageIndex;
                this.setState({
                  containers: containerData,
                  containerNames: containerNames,
                  currentContainerIndex: imageIndex
                });

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "render",
    value: function render() {
      var _this$state = this.state,
          containers = _this$state.containers,
          containerNames = _this$state.containerNames,
          action = _this$state.action,
          inChildProcess = _this$state.inChildProcess,
          currentContainerIndex = _this$state.currentContainerIndex;

      if (!containers) {
        return _react["default"].createElement("div", null, "Loading...");
      }

      if (containers && containers.length < 1) {
        return _react["default"].createElement("div", null, "There are no active docker containers");
      }

      if (!action) {
        return _react["default"].createElement(_ink.Box, {
          flexDirection: "column"
        }, _react["default"].createElement(_ink.Box, null, "Select an action"), _react["default"].createElement(_inkSelectInput["default"], {
          items: ACTIONS,
          onSelect: this.onSelectAction
        }));
      }

      if (inChildProcess) {
        return _react["default"].createElement(_ink.Box, null, _react["default"].createElement(_ink.Color, {
          green: true
        }, "Going into child process... "), _react["default"].createElement(_ink.Color, {
          red: true
        }, "CTRL+D to exit"));
      }

      return _react["default"].createElement(_ink.Box, {
        flexDirection: "column"
      }, action === 'remove' ? _react["default"].createElement(_ink.Box, null, "Select a Container to Remove:") : _react["default"].createElement(_ink.Box, null, "Select a Container to bash into:"), _react["default"].createElement(_inkSelectInput["default"], {
        items: containers,
        onSelect: this.onSelectContainer,
        initialIndex: currentContainerIndex
      }));
    }
  }]);

  return Remove;
}(_react.Component);

(0, _ink.render)(_react["default"].createElement(Remove, null));