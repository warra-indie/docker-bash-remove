"use strict";

var _path = require("path");

var _dockerode = _interopRequireDefault(require("dockerode"));

var _fs = require("fs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var docker = new _dockerode["default"]();

var getInitialContainerData = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var imageName, imageIndex, containers, directory, filePath, fileStat, data, text, json, containerData, containerNames;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            imageName = undefined;
            imageIndex = 0;
            _context.next = 4;
            return docker.listContainers();

          case 4:
            containers = _context.sent;
            directory = process.cwd();
            filePath = (0, _path.join)(directory, 'package.json');
            _context.next = 9;
            return _fs.promises.stat(filePath);

          case 9:
            fileStat = _context.sent;

            if (!fileStat.isFile()) {
              _context.next = 17;
              break;
            }

            _context.next = 13;
            return _fs.promises.readFile(filePath);

          case 13:
            data = _context.sent;
            text = data.toString('utf-8');
            json = JSON.parse(text);

            if (json.config && json.config.docker_image_name) {
              imageName = json.config.docker_image_name;
            }

          case 17:
            containerData = containers.map(function (item, i) {
              if (imageName && imageName === item.Image) {
                imageIndex = i;
              }

              return {
                label: item.Image,
                value: item.Id
              };
            });
            containerNames = containerData.map(function (item) {
              return item.name;
            });
            return _context.abrupt("return", {
              containerData: containerData,
              containerNames: containerNames,
              imageIndex: imageIndex
            });

          case 20:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getInitialContainerData() {
    return _ref.apply(this, arguments);
  };
}();

var getContainer = function getContainer(id) {
  return docker.getContainer(id);
};

module.exports = {
  getInitialContainerData: getInitialContainerData,
  getContainer: getContainer
};