# Install
`npm i -g git+ssh://git@bitbucket.org/warra-indie/docker-bash-remove.git`


# Usage
`docker-bash-remove`

# Automatically selecting your Docker Image
To get docker-bash-remove to select the docker image for this folder, add a config object to your package.json:
```
"config": {
  "docker_image_name": "pdf-generator:latest"
},
"scripts": {
  "dbuild": "docker build -t $npm_package_config_docker_image_name .",
  "drun": "docker run -d -p 80:80 $npm_package_config_docker_image_name",
  "dstart": "yarn dbuild && yarn drun",
},
```

then run `docker-bash-remove` in the same folder
