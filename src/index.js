import '@babel/polyfill'
import cp from 'child_process'
import React, { Component } from 'react'
import  { render, Box, Color } from 'ink'
import SelectInput from 'ink-select-input'
import { getInitialContainerData, getContainer } from './container-helpers'


const ACTIONS = [
  {
    label: 'bash into container',
    value: 'bash',
  },
  {
    label: 'remove container',
    value: 'remove',
  },
]

class Remove extends Component {
  state = {
    containers: undefined,
    containerNames: undefined,
    action: undefined,
    inChildProcess: undefined,
    currentContainerIndex: undefined,
  }

  async componentDidMount() {
    const {
      containerData, 
      containerNames,
      imageIndex,
    } = await getInitialContainerData()

    this.setState({
      containers: containerData,
      containerNames,
      currentContainerIndex: imageIndex,
    })
  }

  onSelectAction = async ({label, value}) => {
    console.log('value is', value)
    this.setState({
      action: value
    })
  }

  onSelectContainer = async ({label, value}) => {
    const { action } = this.state
    const activeContainer = getContainer(value)

    if (action === 'remove') {
      await activeContainer.stop()
      await activeContainer.remove()
      console.log("Removed Container!")
      process.exit()
    } else {
      process.on('exit', () => {
        this.setState({
          inChildProcess: true
        }, () => {
          cp.execSync(`docker exec -it ${value} /bin/sh`, {stdio: 'inherit'})
        })
      });

      process.exit()
    }
  }

  render() {
    const { containers, containerNames, action, inChildProcess, currentContainerIndex } = this.state

    if (!containers) {
      return (
        <div>Loading...</div>
      )
    }

    if (containers && containers.length < 1) {
      return <div>There are no active docker containers</div>
    }

    if (!action) {
      return (
        <Box flexDirection="column">
          <Box>Select an action</Box>
          <SelectInput
            items={ACTIONS}
            onSelect={this.onSelectAction}
          />
        </Box>
      )
    }

    if (inChildProcess) {
      return (
        <Box>
          <Color green>Going into child process... </Color><Color red>CTRL+D to exit</Color>
        </Box>
      )
    }

    return (
      <Box flexDirection="column">
        {action === 'remove' ? <Box>Select a Container to Remove:</Box> : <Box>Select a Container to bash into:</Box> }
        <SelectInput
          items={containers}
          onSelect={this.onSelectContainer}
          initialIndex={currentContainerIndex}
        />
      </Box>
    )
  }
}
render(<Remove/>)

