import { join } from 'path'
import Docker from 'dockerode'
import { promises } from 'fs'

const docker = new Docker();

const getInitialContainerData = async () => {
  let imageName = undefined
  let imageIndex = 0
  const containers = await docker.listContainers()
  const directory = process.cwd()
  const filePath = join(directory, 'package.json')
  const fileStat = await promises.stat(filePath)

  if (fileStat.isFile()) {
    const data = await promises.readFile(filePath)
    const text = data.toString('utf-8')
    const json = JSON.parse(text)

    if (json.config && json.config.docker_image_name) {
      imageName = json.config.docker_image_name
    }
  }


  const containerData = containers.map((item, i) => {
    if (imageName && imageName === item.Image) {
      imageIndex = i
    }

    return {
      label: item.Image,
      value: item.Id,
    }
  })

  const containerNames = containerData.map((item) => {
    return item.name
  })

  return {
    containerData, 
    containerNames,
    imageIndex,
  }
}

const getContainer = (id) => {
  return docker.getContainer(id)
}

module.exports = {
  getInitialContainerData,
  getContainer,
}

